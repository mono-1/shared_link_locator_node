const BoxSDK = require('box-node-sdk');

// path to keypair application json file
var sdkConfig = require('../YOUR_APPS_config.json');

// build client
var sdk = BoxSDK.getPreconfiguredInstance(sdkConfig);
var serviceAccountClient = sdk.getAppAuthClient('enterprise');

// parse cli argument input
const regex = /([^\/]+$)/gm;
let shareLink = regex.exec(process.argv.slice(2))
console.log("Searching for share ID: ", shareLink[0])

// define search chunks
let dayCount = 1

// date array
var getDateArray = function(start, end) {
    var
      arr = new Array(),
      dt = new Date(start);
  
    while (dt <= end) {
      arr.push(new Date(dt));
      dt.setDate(dt.getDate() + dayCount);
    }
    return arr;
}

function searchEventStream(startDate, endOffSet, shareID) {
    var endDate = new Date(startDate);
    endDate.setDate(endDate.getDate() + endOffSet);

    // Open log stream from Box
    serviceAccountClient.events.getEnterpriseEventStream({
        startDate: startDate.toISOString(),
        endDate: endDate.toISOString(),
        pollingInterval: 0,
        // chunkSize: 20,
        eventTypeFilter: [serviceAccountClient.events.enterpriseEventTypes.SHARE]
        }, function(err, stream) {
        
        if (err) { // Handle error 
            console.log(err);
        }
    
        stream.on('data', function(event) {
            // Handle the event
            // console.log(event.created_at)
            // totalEventsSearched += 1;

            if (!searching) {
                // console.log("Ending searchg in: " + startDate.toISOString())
                stream.destroy();
            }
            if (event.additional_details['shared_link_id'] == shareID) {
                console.log(event);
                console.log("Closing remaining searches...")
                searching = false;
                stream.destroy();
            } 
        });

        stream.on('end', function (event) {
            // reach end of this search
            // console.log("not found in " + startDate)
        });
    }); 
}

// build date array to search
var d = new Date();
var lastYear = new Date(d.setDate(new Date().getDate() - 364));
var dateArr = getDateArray(lastYear, new Date());

// console.log(dateArr)
var searching = true;
// var totalEventsSearched = 0;

// console.log(dateArr.length);
console.log("Searching...");
console.log(dateArr[0] + " -- " + dateArr[dateArr.length - 1])


dateArr.some(function (value, index, _arr) {
    // console.log(index + ": " + value)
    searchEventStream(value, dayCount, shareLink[0])
});