# Shared Link Locator - Node

Shared Link Locator is a [Node.js](https://nodejs.org/en/) script for recursively searching through a Box admin event log stream for a specific shared link.

## Installation

Use the included package manager [npm](https://www.npmjs.com/) to install foobar.

```bash
npm install
```

## Usage

```bash
node index.js [URL or shareID]
```